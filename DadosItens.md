
# Envio via GET

## Endpoint: /dadosmestre/dadositens 


### Estrutura RESPONSE


```json
{"mt_dadosmestres_dadositens_in": {
   "mercadoria":    [
            {
         "id": "FG126",
         "descricao": "FIN126,MTS-DI,PD,nº série"
      },
            {
         "id": "FG129",
         "descricao": "FIN129,MTS-DI,PD,QM"
      }
   ],
   "embalagem":    [
            {
         "id": "000000000000000297",
         "descricao": "Filme Strech automático 500 x 25 12kg",
         "onugrpemb": ""
      },
            {
         "id": "000000000000000430",
         "descricao": "SELO PET GALVANIZADO 13MM",
         "onugrpemb": ""
      }
   ],
   "onu":    [
            {
         "id": "01541-I",
         "descricao": "Acetona-cianidrina, estabilizada"
      },
            {
         "id": "01546-II",
         "descricao": "Arseniato de amônio"
      }
   ],
   "material_x_onu":    [
            {
         "material": "000000000000000031",
         "onu": ["00005-SE"]
      },
            {
         "material": "000000000000000278",
         "onu": ["01203-II"]
      }
   ]
}}
```

**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadosmestres_dadositens_in": {
    "retorno": [{
       "type": "E",
       "id": "ZTDV",
       "number": "001",
       "message": "Dados não encontrados."
        }]
}}
```

