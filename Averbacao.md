

# Envio via Post

## Endpoint: /averbacao_retorno


### Body:


```json
 {
	"numero": "",
	"serie": "",
	"filial": "",
	"CNPJCli": "",
	"tpDoc": "",
	"averbado": [
		{
			"dhAverbacao": {
				"time": "",
				"timezone": ""
			},
			"protocolo": "",
			"dadosSeguro": [
				{
					"DadosSeguro": {
						"numeroAverbacao": "",
						"CNPJSeguradora": "",
						"nomeSeguradora": "",
						"numApolice": "",
						"tpMov": "",
						"valorAverbado": "",
						"ramoAverbado": "",
						"hashCodeCalc": ""
					}
				}
			],
			"hashCodeCalc": ""
		}
	],
	"hashCodeCalc": ""
}
```


### Estrutura RESPONSE

```json
{"mt_averbacao_response": {"retorno": [{
   "type": "S",
   "id": "ZSD",
   "number": "018",
   "message": "Dados de Averbação gravados com sucesso!"
}]}}
```
