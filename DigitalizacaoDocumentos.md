

# Tipo de documentos:
 - 01 CT-e
 - 02 CT-e Comprovante
 - 03 Vale Frete
 - 04 Comprovante Estadia
 - 05 Doc Otis
 - 06 NF-e


# Envio via Post

## Endpoint: /digitalizacaodocumento 


### Body:

**Os campos desta integação são maiusculos**

```json
{
  "IDDIG": "",
  "NUM_DOCUMENTO": "064824",
  "TIPO_DOC": "01",
  "SEQ_DOC": "0002",
  "SUB_TIPO_DOC": "A1",
  "PATH_DIGITALIZACAO": "\\Inetpub\\wwwroot_coletas\\CONHECIMENTO\\04\\200802\\",
  "DATA_DIGITALIZACAO": "16-02-2008",
  "HORA_DIGITALIZACAO": "10:01:00",
  "USUARIO_DIGITALIZADOR": "spsilva  ",
  "DATA_CONFERENCIA": "16-02-2008",
  "HORA_CONFERENCIA": "12:31:10",
  "USUARIO_CONFERENCIA": "sbrito    ",
  "HASH": "",
  "REF_SAP": "",
  "NDOC_PAI": ""
} 

```


### Estrutura RESPONSE
**A estrutura retorno informará se um registro foi inserido, atualizado ou foi processado com erro**

***Exemplo sucesso***

```json
{"mt_digDoc_response": {
   "IDDIG": 1000358868,
   "DATA_CRIACAO": "2020-09-03",
   "HORA_CRIACAO": "17:38:49",
   "DATA_ALTERACAO": "0000-00-00",
   "HORA_ALTERACAO": "00:00:00",
   "TYPE": "S",
   "ID": "ZSD",
   "NUMBER": "070",
   "MESSAGE": "Dados gravados com sucesso!"
}}
```

***Exemplo erro***
```json
{"mt_digDoc_response": {
   "IDDIG": 0,
   "DATA_CRIACAO": "0000-00-00",
   "HORA_CRIACAO": "00:00:00",
   "DATA_ALTERACAO": "0000-00-00",
   "HORA_ALTERACAO": "00:00:00",
   "TYPE": "E",
   "ID": "ZSD",
   "NUMBER": "073",
   "MESSAGE": "Tipo de documento enviado não esperado pelo SAP."
}}

```

