Esta API foi construída somente para finalizar os eventos já integrados no NIMBI. 


# Tipos de Eventos
- ACCEPTED
- REJECTED
- COLLECTED
- CANCELLED
- UNLOCKED


# Envio via Post

## Endpoint: /coleta/eventoscoleta/finaliza

### Body:

**O campo numeroasn é opcional. Caso nenhum valor seja informado, irá trazer todos os registros disponíveis para o evento.

```json

{
	"evento": "ACCEPTED",
	"asn": [ {
		"numeroasn": "123",
		},
		{
		"numeroasn": "444",
		}
	]
	
}

```


### Estrutura RESPONSE
**A estrutura retorno sempre será retornada, inclusive em caso de sucesso. Neste caso, o campo type retornará S(Sucesso). 

```json
{
    "mt_eventocoleta_response": {
        "retorno": [{
           "type": "E",
           "id": "ZTDV",
           "number": "190",
           "message": "Nenhum evento do tipo ACCEPTED encontrado."
        }]
}}

```

