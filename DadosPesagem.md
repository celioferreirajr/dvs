
# Envio via POST

### Esta API pode ser usada para consulta ou atualização da pesagem.
### O campo TP_EXEC deve receber um dos valores abaixo:
- "X" - Consulta
- "A" - Atualização


## Endpoint: /dadospesagem

### Body


```json
{
    "tp_exec": "A",
    "numero": "15463",
    "serie": "1",
    "cnpj": "07858230000135",
    "chave": "",
    "etq": "000015463001078582300001350021000016890010001006", 
    "usuario": "",
    "peso":"0.0",
    "dtpesagem": "2020-07-21",
    "horapesagem":"13:52:52",
    "pesagens": [
        {
            "pesagem": {
            "volume": "1",
            "peso": "7.88"
        }
    }
    ]
}
```


### Estrutura RESPONSE

```json
{
    "mt_dadospesagem_response": {
        "chave": "31200407858230000135550010000154631015463430",
        "trq": "002100001689",
        "itemtrq": "0000000010",
        "peso": "0.00000000000000 ",
        "pesagens": [
              {
                "pesagem": {
                "volume": "0000000001",
                "peso": "7.880 "
              }
            },
              {
                "pesagem": {
                "volume": "0000000002",
                "peso": "7.880 "
             }
        }
    ],
    }
}
```


**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadospesagem_response": {
    "retorno": [{
       "type": "E",
       "id": "ZTDV",
       "number": "001",
       "message": "Dados não encontrados."
        }]
}}
```

