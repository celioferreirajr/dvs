
# Envio via GET

## Endpoint: /coleta/datas 

## Par�metro: ?numerocoleta=210000234


### Estrutura RESPONSE
**A estrutura retorno ser� preenchida quando algum problema ocorrer**

```json
{
    "mt_eventosColeta_resp": {
       "datatransferencia": "0000-00-00",
       "dataentrega": "0000-00-00",
       "prazoentrega": "0000-00-00",
       "dataviagem": "0000-00-00",
       "dataarmazem": "0000-00-00",
       "datacoleta": "0000-00-00",
       "datacoletagerada": "0000-00-00",
       "datacheckin": "0000-00-00",
       "retorno": [   {
          "type": "E",
          "id": "",
          "number": "202",
          "message": "Nenhum registro encontrado."
   }]
}}
   

```

