
# Envio via GET

## Parâmentro: ?razao=TRANSP

## Endpoint: /dadosmestre/localrazao


### Estrutura RESPONSE


```json
{"mt_dadosmestres_localrazao_in": {"enderecos": [
      {
      "bp_sap_number": 1000000055,
      "nome_parceiro": "TRANSPORTES DELLA VOLPE S A COMERCI",
      "cnpj_cpf": 65298747000113,
      "grupo_economico": "",
      "uf": "MG",
      "cep": "32676-215",
      "cidade": "BETIM",
      "bairro": "Nova Baden",
      "logradouro": "R. Acaiaca 76,"
   },
      {
      "bp_sap_number": 1000000063,
      "nome_parceiro": "TRANSPORTES DELLA VOLPE S A COMERCI",
      "cnpj_cpf": 65298747000113,
      "grupo_economico": "",
      "uf": "MG",
      "cep": "32676-215",
      "cidade": "BETIM",
      "bairro": "Nova Baden",
      "logradouro": "R. Acaiaca 76,"
   }
  ]}
}

```

**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadosmestres_localrazao_in": {"retorno": [{
   "type": "E",
   "id": "ZTDV",
   "number": "007",
   "message": "Dados não encontrados."
}]}}
```

