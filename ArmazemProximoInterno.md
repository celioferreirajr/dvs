

# Envio via Post

## Endpoint: /dadosmestre/armazemproximointerno

### Body:


```json
{
    "cnpj": "06167730000591",
    "estado": "SP"
}
```


### Estrutura do retorno

**A estrutura retorno será preenchida quando nenhum registro for encontrado**



```json
{
	"mt_dadosmestres_armazemproximointerno_in": {								
				"armazemproximo": {
						"id": "",
						"descricao": "",
						"cep": "", 
						"cidade": ""
				},
				"retorno": [ {
					"type": "E",
					"id": "ZTDV",
					"number": "001",
					"message": "Bp não encontrado."
				} ] 
	}
}
```