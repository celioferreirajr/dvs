**Esta API foi construída somente para consultas dos eventos disponíveis no SAP****


# Tipos de Eventos
- ACCEPTED
- REJECTED
- COLLECTED
- CANCELLED
- UNLOCKED


# Envio via Post

## Endpoint: /coleta/eventoscoleta/consulta

### Body:

**O campo numeroasn é opcional. Caso nenhum valor seja informado, irá trazer todos os registros disponíveis para o evento.

```json

{
	"evento": "ACCEPTED",
	"asn": [ {
		"numeroasn": "123",
		},
		{
		"numeroasn": "444",
		}
	]
	
}

```


### Estrutura RESPONSE
**A estrutura retorno será preenchida quando algum problema ocorrer. Veja que o retorno será baseando na ASN que foi informada, se for o caso**

```json
{"mt_eventocoleta_response": {
   "eventoAccepted": [   {
      "trq_id": 99999999,
      "asn": 123,
      "carriageplanneddate": "2020-08-31",
      "carriageplannedhour": "10:10:10",
      "arrivalestimatedate": "2020-09-01",
      "arrivalestimatehour": "09:00:00",
      "observationtext": "Teste",
      "carrierfiscalidentifier": 11111111111111,
      "dangerousformnumber": 1,
      "reclaimnumber": 123,
      "taxnumber": 22222222222222
   }],
   "retorno": [   {
      "type": "E",
      "id": "ZTDV",
      "number": "195",
      "message": "Evento ACCEPTED não encontrado para o ASN 444."
   }]
}}

```

