


# Envio via Post

## Endpoint: /coleta/criaalteraconsultagrande


### Body:


```json
 {
      "coleta": {
        "ncompra": "122246",
        "numeroasn": "N000152780",        
		"contrato": "",
		"operacao": "",
        "idasnnimbi": "1644",
        "quimico": true,
        "cfop": "",
        "centrocusto": "",
        "ignoraregra": false,
        "incoterms": "EXW",
        "numeroformulario": "",
        "dataalteracao": "2020-12-31 10:10:30",
        "datagravacao": "2020-12-31 10:10:30",
        "dataemissao": "2020-12-31 10:10:30",
        "dataestimada": "2020-12-31 10:10:30",
        "datareal": "2020-12-31 10:10:30",
        "dataprocessamento": "",
        "dataprogramacao": "",
        "dataimpressao":"",
        "observaocaocancelamento": "Mercadoria indisponível",
        "observacao": "Coleta: BRENO DOS SANTOS RESENDE (34 99692-9888",
        "copiacoletaorigem": "000000",
        "pedido": "030033303300",
        "documentotransporte":"",
        "datapedido": "2020-12-31 10:10:30",
        "prioridade": "(N)Normal ou (E)Expressa",
        "ajudante": "(S)Sim ou (N)Não",
        "quantidadeajudante": 0,
        "valorestmercadoria": 0,
        "modalidade": "C",        
        "consolidaarmazem": "S",
        "demandafrota": "S",
        "cargadireta":"S",
        "normalimpexp":"N(Normal) EX(Exportação) IM(Importação)",
        "tipocontainerid": 10,
        "finalidade":"Original",
        "ibgelocalentrega":"1234587"
        "nfes": [
          {
            "numero": "0361400",
            "serie": "1"
			"chave": ""
          }
        ],
        "motoristas": [
          {
            "cpf": "37602516549",
            "nome": "JOAO APARECIDO"
          }
        ],
        "veiculos": [
          {
            "placa": "DTE8526",
            "cpfcnpjproprietario": "37602516549",
            "tipo": "T"
          }
        ],
        "armazem": {
          "id": "06",
          "descricao": "Armazem São Paulo"
        },
        "coletatpfrete": {
          "id": "1",
          "descricao": "String"
        },
        "ocorrencia": {
          "id": "00",
          "descricao": "String"
        },
        "tipocarga": {
          "id": "15",
          "descricao": "DEDICADO"
        },
        "tipoveiculo": {
          "id": "3",
          "descricao": "TRUCK"
        },
        "parceiros": [
          {
            "cnpj": "00924429000175",
            "razaosocial": "FERROVIA CENTRO ATLANTICA S/A",
            "id": "5",
            "uf": "MG",
            "cep": "38050070",
            "cidade": "UBERABA",
            "ibge": "1234587",
            "bairro": "PENHA",
            "logradouro": "RUA LIDICE",
            "numero": "22",
            "complemento": "BLOCO A",
            "almoxarifado": "SAO PAULO-SP",
            "ie": "669575197110",
            "grupoeconomico": {
              "id": "0020",
              "descricao": "VALE"
            },
            "tipoparceiro": {
              "id": "CR",
              "descricao": "Remetente da Coleta"
            }
          }
        ],
        "coletaorigem": {
          "id": "4",
          "descricao": "Nimbi"
        },
        "itens": [
          {
            "altura": 33,
            "comprimento": 49,
            "largura": 46,
            "cubagem": 0,
            "peso": 6.62,
            "pesobalanca": 0,
            "pesocobrado": 0,
            "pesoestimadouom": "KG",
            "qtdvolume": 5,
            "volumetotalcubico": 0,
            "sequencia": 0,
            "licencaespeciais": "",
            "cargafragil": "S",
            "onuCodigo": "",
            "dimensaouom": "CMT",
            "embalagem": {
              "id": "63",
              "descricao": ""
            },
            "mercadoria": {
              "id": "54",
              "descricao": "CALCADO SEGURANCA 39 PU BIDENS INJET"
            }
          }
        ],
        "solicitante": {
          "cnpj": "33592510000154",
          "login": "xml",
          "contatos": [
            {
              "nome": "BRENO DOS SANTOS RESENDE",
              "email": "rpereira@engetron.com.br",
              "telefone": "31 33595800",
              "tipo": "C (Coleta) ou (E) Entrega (S) solicitante"
            }
          ]
        },
        "dadosautorizador": {
          "dataautorizacao": "2020-12-31 10:10:30",
          "email": "rpereira@engetron.com.br"
        },
        "importacaoexportacao": {
          "direserva":"478748",
          "refcliente":"teste",
          "navio": "Titanic",
          "portodestino":"Porto de Dubai",
          "observacao":"enviado para teste",
          "agencia": {
            "cnpj":"11111111111111",
            "descricao":"Teste"
          },
          "despachante":{
            "cnpj":"11111111111111",
            "descricao":"Teste",
            "refdespachante":"teste"
          },
          "container":{
            "codigo":"HLXU8357280",
            "tamanho":20,
            "tipo":"HC",
            "tara":20,
            "lacre":"12313",
            "datacoleta":"2020-12-31 10:10:30",
            "dataentrega":"2020-12-31 10:10:30",
            "datadraft":"2020-12-31 10:10:30",
            "datademurragem":"2020-12-31 10:10:30",
            "dataarmazenagem":"2020-12-31 10:10:30"
          },
          "parceiroimpexp":[{
              "cnpj":"5754878725454",
              "descricao":"Teste",
              "endereco":"Rua Lidice",
              "tipo":"E(entrega) C(coleta)"
          }]          
        }
      }
    }

```


### Estrutura RESPONSE

**A estrutura response possui adicionalmente a lista RETORNO, que é apresentada somente em caso de erro de validação ou consulta**

```json
{"mt_coletagrande_response": 
    {
      "coleta": {
        "ncompra": "122246",
        "numeroasn": "N000152780",        
		"contrato": "",
		"operacao": ""
        "idasnnimbi": "1644",
        "quimico": true,
        "cfop": "",
        "centrocusto": "",
        "ignoraregra": false,
        "incoterms": "EXW",
        "numeroformulario": "",
        "dataalteracao": "2020-12-31 10:10:30",
        "datagravacao": "2020-12-31 10:10:30",
        "dataemissao": "2020-12-31 10:10:30",
        "dataestimada": "2020-12-31 10:10:30",
        "datareal": "2020-12-31 10:10:30",
        "dataprocessamento": "",
        "dataprogramacao": "",
        "dataimpressao":"",
        "observaocaocancelamento": "Mercadoria indisponível",
        "observacao": "Coleta: BRENO DOS SANTOS RESENDE (34 99692-9888",
        "copiacoletaorigem": "000000",
        "pedido": "030033303300",
        "documentotransporte":"",
        "datapedido": "2020-12-31 10:10:30",
        "prioridade": "(N)Normal ou (E)Expressa",
        "ajudante": "(S)Sim ou (N)Não",
        "quantidadeajudante": 0,
        "valorestmercadoria": 0,
        "modalidade": "C",        
        "consolidaarmazem": "S",
        "demandafrota": "S",
        "cargadireta":"S",
        "normalimpexp":"N(Normal) EX(Exportação) IM(Importação)",
        "tipocontainerid": 10,
        "finalidade":"Original",
        "ibgelocalentrega":"1234587"
        "nfes": [
          {
            "numero": "0361400",
            "serie": "1"
			"chave": ""
          }
        ],
        "motoristas": [
          {
            "cpf": "37602516549",
            "nome": "JOAO APARECIDO"
          }
        ],
        "veiculos": [
          {
            "placa": "DTE8526",
            "cpfcnpjproprietario": "37602516549",
            "tipo": "T"
          }
        ],
        "armazem": {
          "id": "06",
          "descricao": "Armazem São Paulo"
        },
        "coletatpfrete": {
          "id": "1",
          "descricao": "String"
        },
        "ocorrencia": {
          "id": "00",
          "descricao": "String"
        },
        "tipocarga": {
          "id": "15",
          "descricao": "DEDICADO"
        },
        "tipoveiculo": {
          "id": "3",
          "descricao": "TRUCK"
        },
        "parceiros": [
          {
            "cnpj": "00924429000175",
            "razaosocial": "FERROVIA CENTRO ATLANTICA S/A",
            "id": "5",
            "uf": "MG",
            "cep": "38050070",
            "cidade": "UBERABA",
            "ibge": "1234587",
            "bairro": "PENHA",
            "logradouro": "RUA LIDICE",
            "numero": "22",
            "complemento": "BLOCO A",
            "almoxarifado": "SAO PAULO-SP",
            "ie": "669575197110",
            "grupoeconomico": {
              "id": "0020",
              "descricao": "VALE"
            },
            "tipoparceiro": {
              "id": "CR",
              "descricao": "Remetente da Coleta"
            }
          }
        ],
        "coletaorigem": {
          "id": "4",
          "descricao": "Nimbi"
        },
        "itens": [
          {
            "altura": 33,
            "comprimento": 49,
            "largura": 46,
            "cubagem": 0,
            "peso": 6.62,
            "pesobalanca": 0,
            "pesocobrado": 0,
            "pesoestimadouom": "KG",
            "qtdvolume": 5,
            "volumetotalcubico": 0,
            "sequencia": 0,
            "licencaespeciais": "",
            "cargafragil": "S",
            "onuCodigo": "",
            "dimensaouom": "CMT",
            "embalagem": {
              "id": "63",
              "descricao": ""
            },
            "mercadoria": {
              "id": "54",
              "descricao": "CALCADO SEGURANCA 39 PU BIDENS INJET"
            }
          }
        ],
        "solicitante": {
          "cnpj": "33592510000154",
          "login": "xml",
          "contatos": [
            {
              "nome": "BRENO DOS SANTOS RESENDE",
              "email": "rpereira@engetron.com.br",
              "telefone": "31 33595800",
              "tipo": "C (Coleta) ou (E) Entrega (S) solicitante"
            }
          ]
        },
        "dadosautorizador": {
          "dataautorizacao": "2020-12-31 10:10:30",
          "email": "rpereira@engetron.com.br"
        },
        "importacaoexportacao": {
          "direserva":"478748",
          "refcliente":"teste",
          "navio": "Titanic",
          "portodestino":"Porto de Dubai",
          "observacao":"enviado para teste",
          "agencia": {
            "cnpj":"11111111111111",
            "descricao":"Teste"
          },
          "despachante":{
            "cnpj":"11111111111111",
            "descricao":"Teste",
            "refdespachante":"teste"
          },
          "container":{
            "codigo":"HLXU8357280",
            "tamanho":20,
            "tipo":"HC",
            "tara":20,
            "lacre":"12313",
            "datacoleta":"2020-12-31 10:10:30",
            "dataentrega":"2020-12-31 10:10:30",
            "datadraft":"2020-12-31 10:10:30",
            "datademurragem":"2020-12-31 10:10:30",
            "dataarmazenagem":"2020-12-31 10:10:30"
          },
          "parceiroimpexp":[{
              "cnpj":"5754878725454",
              "descricao":"Teste",
              "endereco":"Rua Lidice",
              "tipo":"E(entrega) C(coleta)"
          }]          
        }
      }
    }
}
```



**Exemplo com a estruturq RETORNO preenchida:**

```json
    {"mt_coletagrande_response": {
   "coleta":    {
      "ncompra": "",
      "numeroasn": "",
      "contrato": "",
      "operacao": "",
      "idasnnimbi": "0000",
      "quimico": "",
      "cfop": "",
      "centrocusto": "",
      "ignoraregra": "",
      "incoterms": "",
      "numeroformulario": "",
      "dataalteracao": "",
      "datagravacao": "",
      "dataemissao": "",
      "dataestimada": "",
      "datareal": "",
      "dataprocessamento": "",
      "dataprogramacao": "",
      "dataimpressao": "",
      "observaocaocancelamento": "",
      "observacao": "",
      "copiacoletaorigem": "",
      "pedido": "",
      "documentotransporte": "",
      "datapedido": "",
      "prioridade": "",
      "ajudante": "",
      "quantidadeajudante": 0,
      "valorestmercadoria": "0",
      "modalidade": "",
      "consolidaarmazem": "",
      "demandafrota": "",
      "cargadireta": "",
      "normalimpexp": "",
      "tipocontainerid": 0,
      "finalidade": "",
      "nfes":       [
                  {
            "numero": "0000000000",
            "serie": "",
            "chave": ""
         },
                  {
            "numero": "0000000000",
            "serie": "",
            "chave": ""
         }
      ],
      "motoristas":       [
                  {
            "cpf": "00000000000",
            "nome": ""
         },
                  {
            "cpf": "00000000000",
            "nome": ""
         }
      ],
      "veiculos":       [
                  {
            "placa": "",
            "cpfcnpjproprietario": "",
            "tipo": ""
         },
                  {
            "placa": "",
            "cpfcnpjproprietario": "",
            "tipo": ""
         }
      ],
      "armazem":       {
         "id": "",
         "descricao": ""
      },
      "coletatpfrete":       {
         "id": "",
         "descricao": ""
      },
      "ocorrencia":       {
         "id": "",
         "descricao": ""
      },
      "tipocarga":       {
         "id": "",
         "descricao": ""
      },
      "tipoveiculo":       {
         "id": "",
         "descricao": ""
      },
      "parceiros":       [
                  {
            "cnpj": "",
            "razaosocial": "",
            "id": "",
            "uf": "",
            "cep": "",
            "cidade": "",
            "ibge": "",
            "bairro": "",
            "logradouro": "",
            "numero": "",
            "complemento": "",
            "almoxarifado": "",
            "ie": "",
            "grupoeconomico":             {
               "id": "",
               "descricao": ""
            },
            "tipoparceiro":             {
               "id": "",
               "descricao": ""
            }
         },
                  {
            "cnpj": "",
            "razaosocial": "",
            "id": "",
            "uf": "",
            "cep": "",
            "cidade": "",
            "ibge": "",
            "bairro": "",
            "logradouro": "",
            "numero": "",
            "complemento": "",
            "almoxarifado": "",
            "ie": "",
            "grupoeconomico":             {
               "id": "",
               "descricao": ""
            },
            "tipoparceiro":             {
               "id": "",
               "descricao": ""
            }
         }
      ],
      "coletaorigem":       {
         "id": "",
         "descricao": ""
      },
      "itens":       {
         "altura": 0,
         "comprimento": 0,
         "largura": 0,
         "cubagem": 0,
         "peso": 0,
         "pesobalanca": 0,
         "pesocobrado": 0,
         "pesoestimadouom": "",
         "qtdvolume": 0,
         "volumetotalcubico": 0,
         "sequencia": 0,
         "licencaespeciais": "",
         "cargafragil": "",
         "onuCodigo": "",
         "dimensaouom": "",
         "embalagem":          {
            "id": "",
            "descricao": ""
         },
         "mercadoria":          {
            "id": "",
            "descricao": ""
         }
      },
      "solicitante":       {
         "cnpj": "",
         "login": "",
         "contatos":          [
                        {
               "nome": "",
               "email": "",
               "telefone": "",
               "tipo": ""
            },
                        {
               "nome": "",
               "email": "",
               "telefone": "",
               "tipo": ""
            }
         ]
      },
      "dadosautorizador":       {
         "dataautorizacao": "",
         "email": ""
      },
      "importacaoexportacao":       {
         "direserva": "",
         "refcliente": "",
         "navio": "",
         "portodestino": "",
         "observacao": "",
         "agencia":          {
            "cnpj": "",
            "descricao": ""
         },
         "despachante":          {
            "cnpj": "",
            "descricao": "",
            "refdespachante": ""
         },
         "container":          {
            "codigo": "",
            "tamanho": "",
            "tipo": "",
            "tara": "",
            "lacre": "",
            "datacoleta": "0000-00-00",
            "dataentrega": "0000-00-00",
            "datadraft": "0000-00-00",
            "datademurragem": "0000-00-00",
            "dataarmazenagem": "0000-00-00"
         },
         "parceiroimpexp":          [
                        {
               "cnpj": "",
               "descricao": "",
               "endereco": "",
               "tipo": ""
            },
                        {
               "cnpj": "",
               "descricao": "",
               "endereco": "",
               "tipo": ""
            }
         ]
      }
   },
   "retorno":    [
            {
         "type": "E",
         "id": "/SCMTMS/TRQ",
         "number": "019",
         "message": "Falta parceiro de negócios para função do parceiro Destinatário"
      },
            {
         "type": "W",
         "id": "/SCMTMS/TRQ",
         "number": "364",
         "message": "Em Container 10 falta o valor p/o tipo de equipamento do grupo de equipamentos CN"
      }
   ]
}}
```
