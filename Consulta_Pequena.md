


# Envio via Post

## Endpoint: /coleta/consultapequena


### Body:


```json
  {
    "datainicial": "2020-01-01",
    "datafinal": "2020-01-10",
    "coleta": "",
    "asn": "",
    "chavenota": "",
    "nota": "",
    "serie": "",
    "ctrc": "",
    "cidadeorigem": "",
    "cidadedestino": "",
    "remetentecnpj": "",
    "destinatariocnpj": "",
    "remetenterazao": "",
    "destinatariorazao": "",
    "codigotipocarga": "",
    "codigotipoveiculo": "",
    "codigoocorrencia": "",
    "documentotransporte": ""
  }

```


### Estrutura RESPONSE


```json
    {
    "consultacoleta_resp": {
        "consultaColeta": [{
           "ocorrencia": [   {
                "id": "",
                "descricao": ""
           }],
           "coleta": "00000000002100002045",
           "data": "2020-08-27 00:00:00",
           "cidadeorigem": "SP 3501152",
           "cidadedestino": "",
           "remetente": "CIA BRASILEIRA DE ALUMINIO",
           "destinatario": "",
           "tipocarga": [   {
                "id": "",
                "descricao": ""
           }],
           "tipoveiculo": [   {
                "id": "",
                "descricao": ""
           }],
           "peso": "0.0",
           "quimico": "N",
           "po": "",
           "prioridade": "Comum",
           "asn": "",
           "modalidade": "Coleta"
        }
    ]}
}
```

**A estrutura retorno será preenchida quando algum problema de validação ocorrer ou quando nenhum registro for encontrado**
**Exemplo RETORNO preenchido:**

```json
 {
    "consultacoleta_resp": {"retorno": [
    {
       "type": "E",
       "id": "",
       "number": "000",
       "message": "Campo data obrigatório"
    }
    ] }
 }
```
