
# Envio via GET

## Parâmentro: ?razaosocial=TRANSP

## Endpoint: /dadosmestre/clienterazao



### Estrutura RESPONSE


```json
{"mt_dadosmestres_clienterazao_in": {"clientes": [
      {
      "cnpj": 23145287000143,
      "razaosocial": "TRANSPORTES DELLA VOLPE S A COMERCI"
   },
      {
      "cnpj": 23145287000143,
      "razaosocial": "TRANSPORTES DELLA VOLPE S A COMERCI"
   }
  ]}
}

```

**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadosmestres_clienterazao_in": {"retorno": [{
   "type": "E",
   "id": "ZTDV",
   "number": "001",
   "message": "Dados não encontrados."
}]}}
```

