
# Envio via POST

## Endpoint: /dadosmestre/armazemproximo


### Body:


```json
{
	"estado": "SP"
}
```

### Estrutura RESPONSE


```json
{  
   "mt_dadosmestres_armazemproximo_in": 
   {
      "armazemproximo": [
      {
      "id": "0001",
      "descricao": "TDV S�o Paulo - SP",
      "cidade": "S�o Paulo",
      "cep": "02174-010"
      } ]
   }
}  

```

**A estrutura retorno ser� preenchida quando algum problema ocorrer**

```json
{
   "mt_dadosmestres_armazemproximo_in": {
      "armazemproximo": [   {
         "id": "",
         "descricao": "",
         "cidade": "",
         "cep": ""
      }],
      "retorno": [   {
         "type": "E",
         "id": "ZTDV",
         "number": "001",
         "message": "Dados n�o encontrados para o par�metro informado."
      }]
}}
```

