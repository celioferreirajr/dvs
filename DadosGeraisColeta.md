
# Envio via GET

## Endpoint: /dadosmestre/dadosgerais 


### Estrutura RESPONSE


```json
{"mt_dadosmestres_dadosgerais_in": {"coleta": {
   "tipocarga":    [
            {
         "id": "00",
         "descricao": "NORMAL"
      },
            {
         "id": "01",
         "descricao": "LOTACAO"
      }
   ],
   "tipoveiculo":    [
      {
         "id": "V01",
         "descricao": "CARRETA"
      },
            {
         "id": "V02",
         "descricao": "3-4"
      }
   ],
   "tipofrete":    [
            {
         "id": "0001",
         "descricao": "COMUM"
      },
            {
         "id": "0002",
         "descricao": "PROJETO"
      }
   ]
}}}

```

**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadosmestres_dadosgerais_in": {
    "retorno": [{
       "type": "E",
       "id": "ZTDV",
       "number": "001",
       "message": "Dados não encontrados."
        }]
}}
```

