
# Tipo de alteração
- AM: Alterar Modalidade
- AO: Alterar Ocorrencia
- AC: Autorizar Coleta
- CC: Cancelar Coleta
- AP: Alterar Prioridade


# Envio via Post

## Endpoint: /coleta/alteracoleta 


### Body:


```json
{
    "coleta":{
        "coleta":"000000",
        "tipoalteracao":"AO",
        "modalidade":"",
        "prioridade":"",
        "ocorrencia":"55"
    }
} 

```


### Estrutura RESPONSE
**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{
   "mt_alteraocorrencia_resp": {
      "tipoalteracao": "",
      "observacao": "",
      "retorno": [   {
         "type": "E",
         "id": "",
         "number": "000",
         "message": "Erro ao processar"
   }]
}}
   

```

