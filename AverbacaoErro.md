


# Envio via Post

## Endpoint: /averbacao_retorno_erro


### Body:


```json
{
	"msg": {
		"erro": {
			"numdoc": "",
			"seriedc": "",
			"filialdoc": "",
			"linhaarq": ""
		},
		"errodetalhe": {
			"codigo": "",
			"valorenviado": "",
			"valoresperado": "",
			"limite": "",
			"desccompleta": "" 
		}
	}
}

```


### Estrutura RESPONSE

```json
{"mt_averbacao_response": {"retorno": [{
   "type": "S",
   "id": "ZSD",
   "number": "018",
   "message": "Dados de Averbação gravados com sucesso!"
}]}}
```
