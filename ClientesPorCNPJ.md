
# Envio via GET

## Parâmentro: ?cnpj=23145287000199

## Endpoint: /dadosmestre/local


### Estrutura RESPONSE


```json
{"mt_dadosmestres_local_in": {"enderecos": [
      {
      "bp_sap_number": 1000000715,
      "nome_parceiro": "TRANSPORTES DELLA VOLPE S A COMERCI",
      "cnpj_cpf": 23145287000143,
      "grupo_economico": "",
      "uf": "MG",
      "cep": "38017-305",
      "cidade": "UBERABA",
      "bairro": "Residencial Doutor Abel Reis",
      "logradouro": "R. Maria de Lourdes Mel, KM 151 MAI,"
      } 
  ]}
}

```

**A estrutura retorno será preenchida quando algum problema ocorrer**

```json
{"mt_dadosmestres_local_in": {
    "retorno": [{
       "type": "E",
       "id": "ZTDV",
       "number": "001",
       "message": "Dados não encontrados."
        }]
}}
```

